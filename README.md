#client:#

*client.c :* main() used to test the libnetfiles library.

*libnetfiles.h :* a library of functions used in the assignment. 

*libnetfiles.c :* used to connect to a server to perform file functions



#server:#
*netfileserver.c* server used to perform file functions such as open, read, write and close.



run ./server first then in a different terminal, run ./client

###Currently working on:###

Currently, there is an example program running where it sends messages back and forth. 
This obviously needs to be updated to send files back and forth as per assignment description.

*Alec:*

- 

*Larry:*

- Completed working on the handle_open function on the server. libnetfiles.c has been modified for testing purposes
- Currently working on handle_close ~80% complete

###TODO:
*mark unfinished sections with TODO in the source code*

- 