#include<sys/types.h>

#ifndef LIBNETFILES_H_
#define LIBNETFILES_H_

typedef struct{
	char response;
	int filedes;	
	size_t size;
    char *buf;
} Packet;

typedef struct{
	int error;
	char *buffer;
	ssize_t bytesread;
} ServerPacket;

//client functions
int netserverinit(const char *hostname);
int netopen(const char *pathname, int flags);
ssize_t netread(int fildes, void *buf, size_t nbyte);
ssize_t netwrite(int fildes, const void *buf, size_t nbyte);
int netclose(int fildes);

//server functions
void handle_open(int socket, char *buffer);
void handle_read(int socket, char *buffer);
void handle_write(int socket, char *buffer);
void handle_close(int socket, char *buffer);
void *connection_handler(void *args);

#endif //LIBNETFILES_H_
