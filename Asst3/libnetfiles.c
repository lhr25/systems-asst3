#include<stdio.h> //printf
#include<stdlib.h> //exit
#include<string.h> //strcpy
#include<netdb.h> //hostent
#include<errno.h> //error checking
#include<fcntl.h> //O_ types
#include<pthread.h> //threading
#include<arpa/inet.h> //internet
#include<netinet/in.h> //internet
#include<sys/socket.h> //sockets
#include<sys/stat.h>
#include<sys/types.h>
#include "libnetfiles.h" //libnetfiles

typedef struct{
    char response;
    int filedes;
    size_t size;
    char *buf;
}  Packet;

char HOST_IP_ADDR[100];
int sock;
struct sockaddr_in server;
char message[1000], server_reply[2000];

/* client functions */

/*
 * checks if hostname of server exists
 * if so, returns 0
 * if host does not exist, returns -1
 */
int netserverinit(const char *hostname)
{
    struct hostent *he;
    struct in_addr **addr_list;
    int i;

    printf("Resolving %s to ip\n", hostname);
    if ( (he = gethostbyname(hostname)) == NULL ){
        herror("gethostbyname");
        perror("Hostname could not be resolved to ip");
        return -1;
    }
    addr_list = (struct in_addr **) he->h_addr_list;
    for (i=0; addr_list[i] != NULL; i++){
        strcpy(HOST_IP_ADDR, inet_ntoa(*addr_list[i]));
        printf("%s resolved to %s\n", hostname, HOST_IP_ADDR);
        break;
    }
    printf("Host exists\n");
    
    return 0;
}

/*
 *
 *
 */
int netopen(const char *pathname, int flags)
{
    printf("Connecting to host %s\n", HOST_IP_ADDR);

    char buf[2000];
    char sel[2];
    Packet p;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1){
        perror("Could not create socket");
        return -1;
    }
    printf("Socket created\n");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(HOST_IP_ADDR);
    server.sin_port = htons(8888);

    if(connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0){
        perror("Connection failed.");
        return -1;
    }
    printf("Connected\n");

    if(flags != O_RDONLY && flags != O_WRONLY && flags != O_RDWR){
        printf("Incorrect filemode flag\n");
        return -1;
    }
    
    sel[0] = 'o';

    if(flags == O_RDONLY)
        sel[1] = 'r';
    else if(flags == O_WRONLY)
        sel[1] = 'w';
    else if(flags == O_RDWR)
        sel[1] = 'b';

    sel[2] = '\0';
    strcpy(&buf[0], sel);
    strcpy(&buf[2], pathname);

    if(send(sock, buf, sizeof(buf), 0) < 0){
        perror("Send failed");
        return -1;
    }
    if(recv(sock, &p, sizeof(p), 0) < 0){
        perror("Receive failed");
        return -1;
    }
    if(p.response == 'e'){
        strerror(p.filedes);
        return -1;
    }
    printf("File descriptor received from server: \n");
    printf("%d\n", p.filedes);

    return p.filedes;
}

/*
 *
 *
 */
ssize_t netread(int filedes, void *buf, size_t nbyte)
{
    printf("Connecting to host %s\n", HOST_IP_ADDR);

    Packet pbuf;
    Packet p;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1){
        perror("Could not create socket");
        return -1;
    }
    printf("Socket created\n");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(HOST_IP_ADDR);
    server.sin_port = htons(8888);

    if(connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0){
        perror("Connection failed.");
        return -1;
    }
    printf("Connected\n");

    pbuf.buf = buf;
    pbuf.filedes = filedes;
    pbuf.size = nbyte;

    if(send(sock, &pbuf, sizeof(pbuf), 0) < 0){
        perror("Send failed");
        return -1;
    }
    if(recv(sock, &p, sizeof(p), 0) < 0){
        perror("Receive failed");
        return -1;
    }
    if(p.response == 'e'){
        strerror(p.filedes);
        return -1;
    }

    return p.size;
}

/*
 *
 *
 */
ssize_t netwrite(int filedes, const void *buf, size_t nbyte)
{
    printf("Connecting to host %s\n", HOST_IP_ADDR);

    Packet pbuf;
    Packet p;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1){
        perror("Could not create socket");
        return -1;
    }
    printf("Socket created\n");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(HOST_IP_ADDR);
    server.sin_port = htons(8888);

    if(connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0){
        perror("Connection failed.");
        return -1;
    }
    printf("Connected\n");

    pbuf.buf = buf;
    pbuf.filedes = filedes;
    pbuf.size = nbyte;

    if(send(sock, &pbuf, sizeof(pbuf), 0) < 0){
        perror("Send failed");
        return -1;
    }
    if(recv(sock, &p, sizeof(p), 0) < 0){
        perror("Receive failed");
        return -1;
    }
    if(p.response == 'e'){
        strerror(p.filedes);
        return -1;
    }

    return p.size;
}

/*
 *
 *
 */
int netclose(int filedes)
{
    printf("Connecting to host %s\n", HOST_IP_ADDR);

    Packet pbuf;
    Packet p;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1){
        perror("Could not create socket");
        return -1;
    }
    printf("Socket created\n");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(HOST_IP_ADDR);
    server.sin_port = htons(8888);

    if(connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0){
        perror("Connection failed.");
        return -1;
    }
    printf("Connected\n");

    pbuf.filedes = filedes;

    if(send(sock, &pbuf, sizeof(pbuf), 0) < 0){
        perror("Send failed");
        return -1;
    }
    if(recv(sock, &p, sizeof(p), 0) < 0){
        perror("Receive failed");
        return -1;
    }
    if(p.response == 'e'){
        strerror(p.filedes);
        return -1;
    }

    return 0;
}



//server functions
void handle_open(int socket, char *buffer){	
    Packet p;
    char mode = '\0';
    char* filename = malloc(sizeof(buffer) * sizeof(char) ); 

    mode = buffer[1];

    strncpy(filename, &buffer[2], (strlen(buffer) - 2));

    printf("Buffer: %s\n", buffer);
    printf("Opening %s\n", filename);

    if(mode == 'r')
        p.filedes = open(filename, O_RDONLY);
    else if(mode == 'w')
        p.filedes = open(filename, O_WRONLY);
    else if(mode == 'b')
        p.filedes = open(filename, O_RDWR);
    else
        p.filedes = -1;

    if(p.filedes == -1){		//error occurred when trying to open file
        perror("Open Error");
        p.response = 'e';	//Set the response to e for error
        p.filedes = errno;
        send(socket, &p, sizeof(p), 0);
    }
    else{
        p.response = 's';	//Set the response to s for success 
        send(socket, &p, sizeof(p), 0);
    }

    free(filename);
}

void handle_read(int socket, char *buffer)
{
    Packet *pReceived = (Packet *)buffer;
    char sendBuff[strlen(buffer) + sizeof(ssize_t) + sizeof(int)];

    ssize_t bytesread;
    bytesread = read(pReceived->filedes, (sendBuff + sizeof(ssize_t) + sizeof(int)), pReceived->size);

    if(bytesread == -1){
        perror("Read Error");
        printf("%d\n", errno);
        memcpy(sendBuff, &errno, sizeof(errno));
    }

    memcpy(sendBuff + sizeof(int), &bytesread, sizeof(ssize_t));

    send(socket, sendBuff, sizeof(sendBuff), 0);
}


void handle_write(int socket, char *buffer)
{
    int filedes = -1;
    char sendBuff[sizeof(int) + sizeof(ssize_t)];
    ssize_t bytesread;
    size_t nbytes;
    const char *message = (buffer + 1 + sizeof(int) + sizeof(size_t));

    memcpy(&filedes, buffer + 1, sizeof(int));
    memcpy(&nbytes, buffer + 1 + sizeof(int), sizeof(size_t));


    bytesread = write(filedes, message, nbytes);

    if(bytesread == -1){
        perror("Write Error");
        printf("%d\n", errno);
        memcpy(sendBuff, &errno, sizeof(errno));
    }

    memcpy(sendBuff + sizeof(int), &bytesread, sizeof(ssize_t));

    send(socket, sendBuff , sizeof(sendBuff), 0);
}


void handle_close(int socket, char *buffer)
{
    Packet p;
    Packet * pReceived = (Packet *)buffer;

    if(close(pReceived->filedes) == -1){
        perror("Close Error");
        p.response = 'e';
        p.filedes = errno;
    }

    send(socket, &p, sizeof(p), 0);
}


void *connection_handler(void *args)
{
    //Get the socket descriptor
    int sock = *(int*)args;
    int read_size;
    char buffer[2000 + 2];


    //Receive a message from client
    while((read_size = recv(sock, buffer, sizeof(buffer), 0)) > 0){
        char selector = buffer[0];

        if(selector == 'o')
            handle_open(sock, buffer);
        else if(selector == 'r')
            handle_read(sock, buffer);
        else if(selector == 'w')
            handle_write(sock, buffer);
        else if(selector == 'c')
            handle_close(sock, buffer);
    }
    if(read_size == 0){
        puts("Client disconnected");
        fflush(stdout);
    }
    else if(read_size == -1){
        perror("recv failed");
    }

    //Free the socket pointer
    free(args);

    return 0;
}

