#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<fcntl.h>
#include<pthread.h>
#include<errno.h>

typedef struct{
    char response;
    int filedes;
    size_t size;
} Packet;

int sock;
struct sockaddr_in server;
char message[1000], server_reply[2000];

int main(int argc, char *argv[])
{
    //Create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1){
        printf("Could not create socket");
        return 1;
    }
    puts("Socket created");

    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 8888 );

    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0){
        perror("connect failed. Error");
        return 1;
    }

    puts("Connected\n");

    Packet p;
    char buffer[2000];

    //Open file
    //Testing read only
    buffer[0] = 'o';
    buffer[1] = 'r';
    strcpy(buffer+2, "text.txt");
    buffer[11] = '\0';
    printf("SEND BUFFER: %s\n", buffer);

    if(send(sock, buffer, sizeof(buffer), 0) < 0){
        puts("send failed");
        return 1;
    }

    if(recv(sock, &p, sizeof(p), 0) < 0){
        puts("recv failed");
        return 1;
    }
    else{
        puts("Successfully received packet");
        puts("Filedes:");
        printf("%d\n", p.filedes);
    }	

    //Read opened file
    p.response = 'r';
    p.size = 2000;
    printf("SEND Response: %c\n", p.response);
    printf("SEND Size: %zu\n", p.size);
    printf("SEND FileDes: %d\n", p.filedes);

    if(send(sock, &p, sizeof(p), 0) < 0){
        puts("send failed");
        return 1;
    }
    else{
        puts("sent");
    }

    char buff[2000000];

    if(recv(sock, buff, sizeof(buff), 0) < 0){
        puts("recv failed");
        return 1;
    }
    else{
        puts("Successfully received packet");

        int error = -1;
        memcpy(&error, buff, sizeof(error));

        ssize_t bytesread = 0;
        memcpy(&bytesread, buff, sizeof(ssize_t));

        char *message = malloc(sizeof (buff));
        memcpy(message, (buff + sizeof(ssize_t) + sizeof(int)), sizeof(bytesread));	
        puts(message);

    }

    //Close opened file
    p.response = 'c';

    if(send(sock, &p, sizeof(p), 0) < 0){
        puts("send failed");
        return 1;
    }
    else{
        puts("sent");

        if(recv(sock, &p, sizeof(p), 0) < 0){
            puts("recv failed");
            return 1;
        }
        else{
            puts("Successfully received packet");
            puts("Filedes:");
            printf("%d\n", p.filedes);
        }
    }
    close(sock);
    return 0;
}

