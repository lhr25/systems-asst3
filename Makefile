CC = gcc
CFLAGS = -Wall -g -pthread
LDFLAGS= -lm -pthread
DEPS = libnetfiles.h
OBJ = libnetfiles.o

.PHONY: all

all: libnetfiles.o client server

libnetfiles.o: libnetfiles.c $(DEPS)
	$(CC) $(CFLAGS) -c libnetfiles.c libnetfiles.h $(LDFLAGS)

client: libnetfiles.o client.c
	$(CC) $(CFLAGS) -o client libnetfiles.o client.c $(LDFLAGS)

server: libnetfiles.o netfileserver.c
	$(CC) $(CFLAGS) -o server libnetfiles.o netfileserver.c $(LDFLAGS)

.PHONY: clean

clean:
	rm -rf *.o *.gch client server



