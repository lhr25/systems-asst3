#include<sys/types.h>

#ifndef __LNF_H__ 
#define __LNF_H__

int nerserverinit(const char *hostname);
int netopen(const char *pathname, int flags);
ssize_t netread(int filedes, void *buf, size_t nbyte);
ssize_t netwrite(int filedes, const void *buf, size_t nbyte);
int netclose(int filedes);

#endif
