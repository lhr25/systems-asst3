#include<stdio.h> //printf
#include<stdlib.h> //exit
#include<pthread.h> //threading
#include<arpa/inet.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>
#include<fcntl.h>
#include<errno.h>
#include<ifaddrs.h>
#include "lnf.h" //libnetfiles

/*
int cleanup(){
    pthread_join();
    closeFile();
    closeSocket();
    return 0;
}
*/

void *tfunc(void *args){
    int csock = (int)args;
    int read_size;
    char buf[2000];

    while((read_size = recv(csock, buf, sizeof(buf), 0)) != 0){
        if(read_size == 0){
            break;
        }
        else if(read_size == -1){
            perror("Recv return error");
            break;
        }
    }
    printf("%s \n", buf);

    free(args);
    return 0;
}

int main(int argc, char **argv){
    int sd, csock, *new_csock;
    struct sockaddr_in srv, clt;
    socklen_t *c = (socklen_t *)sizeof(struct sockaddr_in);

    sd = socket(AF_INET, SOCK_STREAM, 0);
    if(sd == -1){
        perror("Could not create socket");
        return -1;
    }
    printf("Socket created\n");

    srv.sin_family = AF_INET;
    srv.sin_addr.s_addr = INADDR_ANY;
    srv.sin_port = htons(8888);

    if(bind(sd, (struct sockaddr *)&srv, sizeof(srv)) != 0){
        perror("Bind failed");
        return -1;
    }
    printf("Bind completed successfully\n");

    listen(sd, 3);

    while ( (csock = accept(sd, (struct sockaddr *)&clt, c)) ){
        puts("Connection accepted");
    
        new_csock = malloc(sizeof(int));
        *new_csock = csock;

        pthread_t cthread;
        if((pthread_create(&cthread, NULL, tfunc, (void *)new_csock)) != 0){
            perror("Could not create thread");
            return 1;
        }
        //pthread_join(cthread, NULL);
        puts("Handler assigned");
    }

    //TODO: sigint
    return 0;
}
